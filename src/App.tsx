import { BrowserRouter as Router, Route, Routes, useNavigate } from 'react-router-dom';
import { Users, Products, Splash } from './pages';
import { GlobalProvider } from './GlobalContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss'

function App() {
  return (
    <div className='contain'>
      <GlobalProvider>
        <Router>
          <Routes>
            <Route path="/" element={<Splash />}>
              <Route path="/users" element={<Users />} />
              <Route path="/products" element={<Products />} />
            </Route>
            <Route path='*' element={<NotFound />} />
          </Routes>
        </Router>
      </GlobalProvider>
    </div>
  );
}

export default App;



const NotFound = () => {
  const navigate = useNavigate();

  // Redirect to home page or any other page
  navigate('/users');

  return null;
};