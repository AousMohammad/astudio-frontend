import { useState } from 'react';
import { DataTable, Paginations, Filters } from '../components';
import { usePaginationAndFetch } from '../hooks';

const Users = () => {
  const [filter, setFilter] = useState<string | null>(null);
  const { currentPage, pageSize, totalItems, setCurrentPage, setPageSize } = usePaginationAndFetch('https://dummyjson.com/users', filter);

  return (
    <>
      <div className="filter">
        <Filters setFilter={setFilter} setPageSize={setPageSize} />
      </div>
      <div className='content'>
        <DataTable />
      </div>
      <div className="paginations d-flex align-items-end">
        <Paginations currentPage={currentPage} itemsPerPage={pageSize} totalItems={totalItems} onPageChange={setCurrentPage} />
      </div>
    </>
  );
};

export default Users;