import React, { useEffect } from 'react';
import { Outlet, useNavigate, useLocation } from 'react-router-dom';
import { camelCaseToTitleCase } from '../utils';

const Splash = () => {
    const navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        // Navigate to /users only if the user is at the root path
        if (location.pathname === '/') {
            navigate('/users');
        }
    }, [location, navigate]);

    return (
        <>
            <div className='d-flex justify-content-center align-items-center homeNav'>
                <button className={`mx-2 ${location.pathname === "/users" ? 'activeLink' : ''}`} onClick={() => navigate('/users')}>Users</button>
                <button className={`mx-2 ${location.pathname === "/products" ? 'activeLink' : ''}`} onClick={() => navigate('/products')}>Products</button>
            </div>
            <div>
                Home / <span className='activeLocation'>{camelCaseToTitleCase(window.location.pathname.replace("/", ""))}</span>
            </div>
            <Outlet />
        </>
    );
};

export default Splash;
