import React, { createContext, useState, useMemo } from 'react';
import { Products, Users } from './types';
type Data = Products | Users;
interface IContext {
  data: Data;
  setData: React.Dispatch<React.SetStateAction<Data>>;
  filteredData: Data;
  setFilteredData: React.Dispatch<React.SetStateAction<Data>>;
  currentPage: number;
  setCurrentPage: React.Dispatch<React.SetStateAction<number>>;
  pageSize: number;
  setPageSize: React.Dispatch<React.SetStateAction<number>>;
}

export const GlobalContext = createContext<IContext | null>(null);

export const GlobalProvider: React.FC<any> = ({ children }) => {
  const [data, setData] = useState<Data>({} as Data);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(5);
  const [filteredData, setFilteredData] = useState<Data>({} as Data);

  const value = useMemo(() => ({
    data, setData, filteredData, setFilteredData, currentPage, setCurrentPage, pageSize, setPageSize
  }), [data, setData, filteredData, setFilteredData, currentPage, setCurrentPage, pageSize, setPageSize]);





  return (
    <GlobalContext.Provider value={value}>
      {children}
    </GlobalContext.Provider>
  );
};
