import React from 'react';
import { Input } from 'reactstrap';
import { useFilter, usePageSize } from '../hooks';

interface FiltersProps {
    setPageSize: React.Dispatch<React.SetStateAction<number>>;
    setFilter: React.Dispatch<React.SetStateAction<string | null>>;
}

const Filters: React.FC<FiltersProps> = ({ setPageSize, setFilter }) => {
    const { pageSize, handlePageSizeChange } = usePageSize(setPageSize);
    const { isSearchVisible, toggleSearch, handleSearchChange } = useFilter();

    const handleFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const emailQuery = e.target.value;
        setFilter(emailQuery);
    };


    return (
        <div className='d-flex align-items-center'>
            <div className='ms-0 mx-4'>
                <select value={pageSize} onChange={handlePageSizeChange} style={{ marginInlineEnd: '15px' }}>
                    <option value={5}>5</option>
                    <option value={10}>10</option>
                    <option value={20}>20</option>
                    <option value={50}>50</option>
                </select>
                <label>Entries</label>
            </div>
            |
            <div className='mx-4 d-flex'>
                <button className='btn' onClick={toggleSearch}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                    </svg>
                </button>
                {isSearchVisible && <Input className='mx-2' placeholder="search" onChange={handleSearchChange} />}
            </div>
            |
            <div className='mx-4 d-flex'>
                <Input className='mx-2' placeholder="Filter From Server" onChange={handleFilterChange} />
            </div>
        </div>
    );
};

export default Filters;
