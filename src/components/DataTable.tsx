import React, { useContext } from 'react';
import { GlobalContext } from '../GlobalContext';
import { TableHeaders, TableRows } from './Table';
export const MAX_COLUMNS = 11;

const DataTable: React.FC = () => {
    const context = useContext(GlobalContext);

    if (!context) {
        return <div>Loading...</div>;
    }

    const { filteredData, pageSize } = context;

    let currentData: any[] = [];
    let headerData: any[] = [];

    if ('products' in filteredData) {
        currentData = filteredData.products?.slice(0, pageSize) || [];
        headerData = filteredData.products || [];
    } else if ('users' in filteredData) {
        currentData = filteredData.users?.slice(0, pageSize) || [];
        headerData = filteredData.users || [];
    }

    return (
        <table className="table table-responsive table-striped table-bordered">
            <thead>
                <tr>
                    <TableHeaders items={headerData} />
                </tr>
            </thead>
            <tbody>
                <TableRows items={currentData} />
            </tbody>
        </table>
    );
};

export default DataTable;
