import _ from "lodash";
import { MAX_COLUMNS } from "../DataTable";

interface TableRowsProps {
    items: Array<{ [key: string]: any }> | null | undefined;
}

export const TableRows: React.FC<TableRowsProps> = ({ items }) => (
    <>
        {items?.map((item) => (
            <tr key={_.uniqueId()}>
                {Object.values(item).slice(0, MAX_COLUMNS).map((value, index) => (
                    <td key={_.uniqueId()}>{value}</td>
                ))}
            </tr>
        ))}
    </>
);