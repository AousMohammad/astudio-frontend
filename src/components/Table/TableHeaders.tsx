import _ from "lodash";
import { camelCaseToTitleCase } from "../../utils";
import { MAX_COLUMNS } from "../DataTable";

interface TableHeadersProps {
    items: Array<{ [key: string]: any }> | null | undefined;
}

export const TableHeaders: React.FC<TableHeadersProps> = ({ items }) => (
    <>
        {items && items?.length > 0 ? Object.keys(items[0]).slice(0, MAX_COLUMNS).map((key) => (
            <th key={_.uniqueId()}>{camelCaseToTitleCase(key)}</th>
        )) : null}
    </>
);