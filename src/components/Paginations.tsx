import React, { useState } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
interface PaginationProps {
    totalItems: number;
    itemsPerPage: number;
    currentPage: number;
    onPageChange: (page: number) => void;
}
const Paginations: React.FC<PaginationProps> = ({ totalItems, itemsPerPage, onPageChange }) => {
    const totalPages = Math.ceil(totalItems / itemsPerPage);
    const [activePage, setActivePage] = useState(0);
    return (
        <Pagination>
            {Array.from({ length: totalPages }, (_, index) => (
                <PaginationItem key={index}>
                    <PaginationLink className={activePage === index ? 'activePage' : ''} onClick={() => { setActivePage(index); onPageChange(index + 1) }}>
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>
            ))}
        </Pagination>
    );
};

export default Paginations;
