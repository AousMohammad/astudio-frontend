export function camelCaseToTitleCase(str: string) {
    // Check if the string is a single word (no camelCasing)
    if (!/[a-z][A-Z]/.test(str)) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    // Replace camelCase with space-separated words
    const result = str.replace(/([a-z])([A-Z])/g, '$1 $2');

    // Capitalize the first letter of each word
    return result.replace(/\b\w/g, char => char.toUpperCase());
}
