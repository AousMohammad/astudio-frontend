import { useContext, useEffect, useState } from 'react';
import { GlobalContext } from '../GlobalContext';
import axios from 'axios';

const usePaginationAndFetch = (apiEndpoint: string, searchQuery: string | null = null) => {
    const context = useContext(GlobalContext);

    if (!context) {
        throw new Error("usePaginationAndFetch must be used within a GlobalProvider");
    }

    const { setData, currentPage, setCurrentPage, pageSize, setPageSize, setFilteredData } = context;
    const [totalItems, setTotalItems] = useState(0);

    useEffect(() => {
        let url = '';
        if (searchQuery) {
            url = `${apiEndpoint}/search?skip=${currentPage === 1 ? 0 : ((currentPage - 1) * pageSize)}&limit=${pageSize}&q=${searchQuery}`;
        } else {
            url = `${apiEndpoint}?skip=${currentPage === 1 ? 0 : ((currentPage - 1) * pageSize)}&limit=${pageSize}`;
        }
        axios.get(url)
            .then((data) => {
                setTotalItems(data.data.total);
                setData(data.data);
                setFilteredData(data.data);
            });
    }, [currentPage, pageSize, searchQuery, apiEndpoint]);

    return {
        currentPage,
        pageSize,
        totalItems,
        setCurrentPage,
        setPageSize,
    };
};


export default usePaginationAndFetch;
