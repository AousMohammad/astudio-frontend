import { useState, useContext } from 'react';
import { GlobalContext } from '../GlobalContext';

export const useFilter = () => {
    const [isSearchVisible, setIsSearchVisible] = useState(false);
    const context = useContext(GlobalContext);

    if (!context) {
        throw new Error("useFilter must be used within a GlobalProvider");
    }

    const { data, setFilteredData } = context;

    const toggleSearch = () => {
        setIsSearchVisible(!isSearchVisible);
    };

    const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const query = e.target.value.toLowerCase();

        let newFilteredData: any[] = [];

        if ('products' in data) {
            newFilteredData = data.products?.filter(product =>
                Object.values(product).some(value =>
                    value?.toString().toLowerCase().includes(query)
                )
            ) || [];
        } else if ('users' in data) {
            newFilteredData = data.users?.filter(user =>
                Object.values(user).some(value =>
                    value?.toString().toLowerCase().includes(query)
                )
            ) || [];
        }

        setFilteredData({ ...data, products: newFilteredData });
    };

    return { isSearchVisible, toggleSearch, handleSearchChange };
};

