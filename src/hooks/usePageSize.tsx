import { useState } from 'react';

export const usePageSize = (setPageSize: any, initialSize = 5) => {
    const [pageSize, _setPageSize] = useState(initialSize);

    const handlePageSizeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const newSize = parseInt(e.target.value, 10);
        setPageSize(newSize);
        _setPageSize(newSize);
    };

    return { pageSize, handlePageSizeChange };
};
